# Gitlab project template for R package

This project is useful to initiate a new Gitlab project containing a R package. (This can also be useful for simple R code project -not package- because this allow usage of devtools commands for tests, quality, etc.)

## Prerequisites

Optional:

- [TortoiseGit](https://tortoisegit.org/) installed on Windows ([download link](https://download.tortoisegit.org/tgit/2.13.0.0/TortoiseGit-2.13.0.1-64bit.msi)). This will simplify some steps.
- Knowledge of *Git* and *Gitlab* (UREP docs : [Git](https://sites.inra.fr/site/urep/Wiki%20UREP/git.aspx), [Gitlab](https://sites.inra.fr/site/urep/Wiki%20UREP/GitLab.aspx)).

## How to use

1. Clone this project locally on your computer
2. Create a new "empty project" (without README file) on the [Forgemia (Gitlab) website](https://forgemia.inra.fr/urep). The project must be created at the right place in the groups and the right name, if you don't know which group is appropriate, then ask Raphael Martin with an issue in [Global issues](https://forgemia.inra.fr/urep/general/global-issues) or directly.
3. Clone this new project locally on your computer
4. Copy all files and folders (except `.git` folder) from the first clone into the second (which is empty except `.git` folder)
5. You can remove the clone you done for [this project](https://forgemia.inra.fr/urep/dev_utils/gitlab-project-templates/r-package-project-template), it is no longer useful.
6. Select all copied files and folders and then use a right mouse click on them. In the displayed menu over `TortoiseGit`, and in the submenu select `Add...`.
7. Commit this files, for that use a right mouse click on your base clone folder and select `Git Commit -> "main"...`. In the new window select in bottom right buttons select "Commit" (the button have an arrow to change the button action from "Commit & Push" to "Commit"). At this stage you have commit your changes in your local git repository, nothing is sent to Gitlab.
8. Follow instructions in `newProjectCommands.R` file to adjust the project to your needs...
9. Add file to git and commit again your changes. (see steps 6. and 7. for details)
10. Push all your commits to Gitlab. For that use a right mouse click on them. In the displayed menu over `TortoiseGit`, and in the submenu select `Push...`, then validate the new window without changing anything.
11. Now you can work on your projects with the help of commands in `devCommands.R`.

> Gitlab provide the possibility to add custom project templates for project creation. Unfortunately, this is a paid functionality, so we don't have access to it.

### Package management guidelines

Some web guidelines for R package can be found here:

- [R Packages web book](https://r-pkgs.org/) by Hadley Wickham and Jenny Bryan.
- [Official R package documentation](https://cran.r-project.org/doc/manuals/R-exts.html#Creating-R-packages)
  can be hard to understand for beginners.
- [The devtools package documentation](https://devtools.r-lib.org/index.html)
